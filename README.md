Hortonworks repository local mirror
=========

Creates and configures subj for CentOS 7.

> Local repositories are frequently used in enterprise clusters that have limited outbound internet access. In these scenarios, having packages available locally provides more governance, and better installation performance. These repositories are used heavily during installation for package distribution, as well as post-install for routine cluster operations such as service start/restart operations. [Using a Local Repository](https://docs.hortonworks.com/HDPDocuments/Ambari-2.2.2.0/bk_Installing_HDP_AMB/content/_using_a_local_repository.html).


Requirements & Dependencies
------------

No

Role Variables
--------------

No

TODO
--------------

* move hardcoded values to variables
* add support os other OSes

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servername
      become: yes
      gather_facts: no
    
      roles: 
        - hortonworks_local_mirror


License
-------

BSD

Author Information
------------------

Daniil A. Sabadash (daniil@sabadash.su)
